%{
Transformation matrix: rotation + translation.
No checks.
Params:
	R: 2x2 rotation matrix
	t: 2x1 translation vector
Returns:
	3x3 homogeneous matrix
%}

function T = Tm(R, t)
	T = eye(3);
	T(1:2,1:2) = R;
	T(1:2,3) = t;
end
