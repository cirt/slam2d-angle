%{
2D rotation matrix
Params:
	theta: scalar angle
Returns:
	2x2 matrix
%}

function m = rot(theta)
	s = sin(theta);
	c = cos(theta);
	m = [c, -s;
	     s,  c];
end
