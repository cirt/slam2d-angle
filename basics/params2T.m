%{
Transformation matrix from minimal parametrization
Params:
	x: 3x1 [x,y,th] parameters, where [x,y] is the displacement and th is the
		angle of the rotation.
Returns:
	3x3 homogeneous matrix
%}

function T = params2T(x)
	T = Tm(rot(x(3)), x(1:2));
end
