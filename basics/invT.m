%{
Inverse of a transformation matrix.
Params:
	T: 3x3 transformation matrix
Returns:
 3x3 inv(T)
%}

function Ti = invT(T)
	Rt = T(1:2,1:2)';
	Ti = eye(3);
	Ti(1:2,1:2) = Rt;
	Ti(1:2,3) = -Rt * T(1:2,3);
end
