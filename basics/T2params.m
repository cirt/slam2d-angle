%{
Extract the minimal parametrization from the transformation matrix.
Params:
	T: 3x3 transformation matrix
Returns:
	3x1 vector of parameters [x,y,th]
%}

function x = T2params(T)
	th = atan2(T(2,1), T(1,1));
	x = [T(1:2,3); th];
end
