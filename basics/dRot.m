%{
Derivative of 2D rotation matrix
Params:
	theta: scalar angle
Returns:
	2x2 matrix
%}

function m = dRot(theta)
	s = sin(theta);
	c = cos(theta);
	m = [-s, -c;
	     c,  -s];
end
