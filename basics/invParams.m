%{
Transform parameters to parameters of the inverse transformation.
Since the state contains the inverse transformation, this function can 
be used to obtain more interpretable values.
Params:
	x: 3x1 [x,y,th]
Returns
	x: 3x1 [x,y,th]
%}

function xi = invParams(x)
	xi = T2params(invT(params2T(x)));
end
