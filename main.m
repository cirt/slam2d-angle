%{
Main script file.
It initilizes the variables, loads the dataset and calls the other functions.
It loops for each timestep.
%}


% Definitions
addpath 'basics'
addpath 'model'
addpath 'generic'
addpath 'tools/g2o_wrapper'
addpath 'tools/visualization'

% Load the dataset
%   NOTE: landmarks loaded just for plotting; never passed to minimizeLS
[landmarks, ~, transitions, observations] = ...
		loadG2o('dataset/slam-2d-bearing-only.g2o');

% Dataset checks
[observations, transitions] = datasetCheck(observations, transitions);

% Prepare the landmarks IDs. NOTE: this is done offline, yet.
global l2i;
global i2l;
[l2i, i2l] = assignLandmarksIndices(observations);

% Initialization
global posesN  % Number of robot poses in the state vector
posesN = 1;    % Initially, just one pose
maxPosesN = 30; % Maximum number of poses in the state vector
x = zeros(length(i2l)*2+3,0); % No previous state (but save the length)
saveState();   % Clear history
converged = false;

% For each timestep
for i = 1:length(observations)
	printf('\nStep %i\n', i);

	% Use odometry to estend the initial guess
	%	  NOTE: assuming that observations(i) is reached after transitions(i)
	xInit = statePredict(x, transitions(i), posesN < maxPosesN, converged);

	% Update the range of poses to estimate inside the state
	firstPoseI = max(1, i-maxPosesN+1);
	posesRange = firstPoseI:i;
	posesN = length(posesRange);

	% Triangulate from poses to estimate landmark positions
	if converged
		xInit = landmarksPredict(xInit, observations(posesRange));
	end

	% Solve Least Squares
	[x,chi,P,converged] = minimizeLS(xInit, observations(posesRange),
			transitions(posesRange));
	if !converged
		x = xInit
	end

	% Save
	saveState(invState(x));

	% Plot
	cla;
	drawFullState(x, landmarks);
	pause(0.2);
end

