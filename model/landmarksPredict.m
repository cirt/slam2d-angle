%{
For each getLocalizableLandmarks(), it predicts a probable landmark position,
computed with triangulation. Usually, there are more than 2 bearing
observations for each localizable landmark. So, it returns the minimum error
solution.

Params:
	x: state vector (see stateIdx)
	observations: array of struct of an observations (see loadG2o)

Returns:
	state with upted landmarks. Poses are not modified.
%}

function xL = landmarksPredict(x, observations)

global l2i;
global i2l;

% Cell array of matrices: landmark_sequentialID x line_equation x coefficient
% Each line is a bearing measurement of a landmark
equations = cell(length(i2l),1);

% For each pose that generated this set of measures
for i = 1:length(observations)

	% Transf matrix
	xPose = x(stateIdx(i));
	TInv = invT(params2T(xPose));

	% For each bearing measure
	for o = observations(i).observation

		% Two points along the bearing in world frame
		p2 = TInv * Tm(rot(o.bearing), [0;0]) * [1;0;1];
		p1 = TInv(1:2,3);

		% Equation of this line
		a = p2(2) - p1(2);
		b = p1(1) - p2(1);
		c = p1(2)*p2(1) - p1(1)*p2(2);

		equations{l2i(o.id)} = [equations{l2i(o.id)}; a,b,c];
	end
end

% Filter landmarks to localize
landIDs = getLocalizableLandmarks(observations);
for i = 1:length(equations)
	if !isempty(equations{i}) && !any(landIDs == i2l(i))
		equations{i} = zeros(0); % delete
	end
end

% Solve with minimum error solution
estimatedLandms = sparse(2, length(equations)); % points column-wise
for i = 1:length(equations)
	if !isempty(equations{i})
		coefficients = equations{i};
		A = coefficients(:,1:2);
		b = -coefficients(:,3);
		estimatedLandms(:,i) = A\b;
	end
end

% Update
xL = x;
for i = 1:length(estimatedLandms)
	if norm(estimatedLandms(:,i), 'inf') > 0
		xL(stateIdx(-i)) = estimatedLandms(:,i);
	end
end

end
