%{
Observation model of the landmarks measurements. Bearing angle of a landmark
with respect to robot position and Jacobian of this function with respect to x.
Params:
	x: 5x1 pose and landmark configuration vector [x,y,th, xL, yL]'
Returns:
	z: Predicted angle of the measure; scalar value.
	J: 1x3 Jacobian of this function, computed at x.
%}

function [z, J] = landmarkMeasureAndJacobian(x) 

	% Split
	xPose = x(1:3);
	xLand = x(4:5);

	% h(x) observation
	p_r = params2T(x) * [xLand; 1];% x is already an inverse transformation, r->0
	if p_r(1) == 0 && p_r(2) == 0
		xLand(1) += 0.1;  % Avoid 0 division 
		p_r = params2T(x) * [xLand; 1];
	end
	py = p_r(2);
	px = p_r(1);
	z = atan2(py, px);

	% Jh(x) jacobian
	JAtan2 = [-py, px] .* (1 / (px*px + py*py));
	JT = [eye(2), dRot(x(3))*xLand, rot(x(3))];
	J = JAtan2 * JT;
end
