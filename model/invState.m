%{
Return the state vector with all parameters inverted.
This applies invParams(), for each SE2 pose in the state vector.
See stateIdx.

Params:
	x: the full state vector (defined in stateIdx())
Returns:
	the state vector with poses inverted.
%}

function xInv = invState(x)

	global posesN

	xInv = x;
	for i = 1:posesN   % Not touching tail of x
		xInv(stateIdx(i)) = invParams(x(stateIdx(i)));
	end

end
