%{
Make a prediction of the next state vector, given the previous and the odometry
measure. Calls posePredict for the most recent pose and leave the other
component as they are. Reads global variable posesN.

Params:
	x: previous state vector (see stateIdx)
	transition: struct of the odometry measure (see loadG2o)
	extend: boolean value. If true, the state vector is extended; if false,
			poses are shifted to the left.
	converged: if true, the state x is used; otherwise the last converged state.

Returns:
	state vector updated
%}

function xNext = statePredict(x, transition, extend, converged)

	global posesN
	N = posesN;

	persistent xConverged;

	if isempty(x)
		% TODO: move initialization out of this function
		lengthOfX = size(x, 1);
		xNext = zeros(lengthOfX,1);
		xNext(3) = -pi/2; % Align initial pose with map

		% Landmarks at random, far
		radius = 20;
		angles = rand((lengthOfX-3)/2,1)*2*pi-pi;
		c = radius*cos(angles);
		s = radius*sin(angles);
		xNext(4:end) = reshape([c,s]',[],1);

	else

		if !converged
			x = xConverged;
		end

		lastPose = x(stateIdx(N));
		nextPose = posePredict(lastPose, transition);

		if extend
			xNext = [x(1:N*3); nextPose; x(N*3+1:end)];
		else
			xNext = [x(4:N*3); nextPose; x(N*3+1:end)];
		end

		xConverged = xNext; % estimates can be always reused
	end
end
