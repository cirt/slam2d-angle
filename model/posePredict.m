%{
Make a prediction of the next robot pose, given the previous and the odometry
measure. If x is empty, it is initialized with a constant.

Params:
	x: 3x1 previous pose
	transition: struct of the odometry measure (see loadG2o)

Returns:
	3x1 point estimate of the next pose
%}

function xNext = posePredict(x, transition)

TRobot = params2T(x); % world w.r.t. robot at time i
TStep = invT(params2T(transition.v)); % bring to inverse transformations
TNext = TStep * TRobot; % world w.r.t. robot at time i+1

xNext = T2params(TNext);

end
