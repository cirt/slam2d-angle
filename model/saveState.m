%{
Save the state vector and the past trajectory to a text file. 
A file in 'output/trajectory.txt' contains, one per row, the history 
of configurations (each time, just the most recent pose is saved).
A file in 'output/lastState.txt' contains last state vector x passed.
Whithout parameters, the history is cleared.
A file in 'output/landmarks.txt' contains the landmarks positions in each row.
Each time, this is overwritten with the most recent estimate.
NOTE: 2 decimals of precision becuase it's just for visualization.

Params:
	x: the state vector (see stateIdx)
%}

function saveState(x)

trajectoryFilePath = 'output/trajectory.txt';
lastStateFilePath = 'output/lastState.txt';
landmarksFilePath = 'output/landmarks.txt';

% Vars
persistent xTrajectory
global posesN

% Clear whithout arguments
if nargin == 0
	xTrajectory = zeros(0, 3);
	return
end

% Append the most recent pose
persistent xTrajectory = zeros(0, 3); % 3 is the length of each pose
xTrajectory(end+1,:) = x(stateIdx(posesN));

% Landmarks
landmarks = reshape(x(3*posesN+1:end), [], 2);

% Write
dlmwrite(trajectoryFilePath, xTrajectory, 'delimiter', '  ',
		'precision', '%.2f', 'newline', 'unix');
dlmwrite(lastStateFilePath, x, 'delimiter', '  ',
		'precision', '%.2f', 'newline', 'unix');
dlmwrite(landmarksFilePath, landmarks, 'delimiter', '  ',
		'precision', '%.2f', 'newline', 'unix');

end
