%{
Observation model of the odometry measurements. The odometry measure contains
the parameters of the relative transformation from the previous to the next
(classic, non inverse).

Params:
	x: 6x1 vector of two consecutive poses from which the odometry is predicted.

Returns:
	3x1 parameters of the relative transformation matrix
	3x6 Jacobian of this function, computed at x.
%}

function [z, J] = odometryMeasureAndJacobian(x)
	
	% Aliases for simplicity
	x1 = x(1); y1 = x(2); t1 = x(3);
	x2 = x(4); y2 = x(5); t2 = x(6);
	c12 = cos(t1 - t2);
	s12 = sin(t1 - t2);

	% Expression of z in closed form
	z = [
	    -c12 * x2 + s12 * y2 + x1;
	    -s12 * x2 - c12 * y2 + y1;
			atan2(s12, c12)   % minimum phase equivalent of: (t1 - t2)
	];
	% This ^ is equivalent to:
	%T1 = params2T(x(1:3));
	%T2 = params2T(x(4:6));
	%dT = T1 * invT(T2);
	%z = T2params(dT)
	% but it's also differentiable

	% Jacobian of z
	J1 = [
	    1, 0, s12 * x2 + c12 * y2;
	    0, 1, -c12 * x2 + s12 * y2;
	    0, 0, 1
	];
	J2 = [
	    -c12, s12,  -J1(1,3);
	    -s12, -c12, -J1(2,3);
	    0,    0,    -J1(3,3)
	];
	J = [J1, J2];

end
