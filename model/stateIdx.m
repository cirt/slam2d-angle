%{
This function returns the positions in the state vectors.
This defines how elements are arranged in the full state. It can be used to 
access single fields and set them.
The state is composed by posesN robot poses and L landmark 2d positions:
[x1, y1, th1, x2, y2, th2, ... thN, lx1, ly1, ... lxL, lyL]'
Each pose is a word configuration with respect to the robot: inverse transf.
This functtion reads the global variable 'posesN' which is the number of poses
([x,y,th] blocks) the state vector contains.

Params:
	idx: positive index or vector of indices of the desired pose blocks. Negative
		integers indicate landmark positions blocks.
	
Return:
	Row vector of indices (a range)	
%}

function idxRange = stateIdx(idx)

	global posesN
	iBase = posesN*3+1;   % First landmark position

	% For each input
	idxRange = zeros(1,0);
	for i = idx

		if i > 0 && i <= posesN    % pose block
			ii = (i-1)*3+1;
			idxRange = [idxRange, ii:ii+2];
		elseif i < 0               % landmark block
			ii = (-i-1)*2+iBase;
			idxRange = [idxRange, ii:ii+1];
	end
end
