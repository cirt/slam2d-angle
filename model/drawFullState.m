%{
Draws the full vector state in a plot.
See model/stateIdx for a description of how the state is composed.
Reads the global variable 'posesN'.

Params:
	x: state vector
	landmarks: array of struct of landmark positions (see loadG2o) (optional)
%}

function drawFullState(x, landmarks)
hold on

axis([-14, 14, -14, 14]);
axis equal;

global posesN;
global i2l;

x = invState(x);

% Draw trajectory
xPoses = x(1:3*posesN);
trajectory = reshape(xPoses, 3, posesN);
for i = 1:size(trajectory,2)-1
	plot([trajectory(1,i)], [trajectory(2,i)], 'k', 'marker', '.', 'markersize',
			10);
	plot([trajectory(1,i), trajectory(1,i+1)], [trajectory(2,i),
			trajectory(2,i+1)], 'k', 'linewidth', 2);
end

% Draw last pose
drawRobot(x(stateIdx(posesN)), zeros(3));

% Draw real landmarks if given
if nargin >= 2
	drawLandmarks(landmarks);
end

% Draw estimated landmarks
landmarksN = (length(x)-3*posesN)/2;
for i = 1:landmarksN
	landmarksEst(i) = landmark(i2l(i), x(stateIdx(-i)));
end
for land = landmarksEst
	drawLandmarks(land, 'b', 'fill');
end

hold off
end
