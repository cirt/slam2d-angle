%{
Given a set of observations, assigns a sequential id to each landmark.
Then, it returns the landmark to int and int to landmark maps. New IDs are 
consecutive, starting from 1, because they are an ordering and they can be
used as indexing in a vector. Maps are sparse matrices.

Params:
	observations: array of struct of observations (see loadG2o)

Returns:
	l2i: sparse vector, landmark id to integer in the ordering
	i2l: sparse vector, position to landmark id
%}

function [l2i, i2l] = assignLandmarksIndices(observations)

	% Copy all IDs and spot errors
	allLandmarks = zeros(1,0);
	maxId = 0;
	for obs = observations
		for o = obs.observation
			if o.id < 1
				error('Landmarks must have positive (> 1) IDs.');
			end
			if o.id > maxId
				maxId = o.id;
			end

			allLandmarks(end+1) = o.id;  % Save (with duplicates)
		end
	end

	% List
	i2l = sort(unique(allLandmarks));

	% Invert
	l2i = sparse(1,maxId);
	for i = 1:length(i2l)
		l2i(i2l(i)) = i;
	end

end
