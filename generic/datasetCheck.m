%{
This algorithm uses an eucledean parametrization for every variable. However,
this approximation is not valid for 180deg turns. This method simply splits
big rotations in two smaller transitions. A robot could also do this
authomatically. In my dataset it happens only twice.

Params:
	observations: 1xN array of struct of observations (see loadG2o)
	transitions: 1xN array of struct of odometry measures (see loadG20)

Returns:
	obs: the same observations with some additions (if needed)
		A single observation with 0 precision is added to fill the gap
	trans: the same transitions with some additions (if needed)
%}

function [obs, trans] = datasetCheck(observations, transitions)

	obs = observations;
	trans = transitions;

	% Check: transitions(i) must lead to observations(i).pose
	if length(observations) != length(transitions)
		error('Transitions and observations must have equal length.');
	end

	% Find the first free pose ID
	nextPoseId = 0;
	for o = observations
		nextPoseId = max(nextPoseId, o.pose_id);
	end
	++nextPoseId;

	% Scan each transition: assuming odometry is good enough for this purpose
	transLength = length(trans);
	i = 1;
	while i <= transLength

		% If too big
		t = trans(i);
		if abs(t.v(3)) > 2.36 % ~pi*3/4

			% Create the first half
			newId = nextPoseId++;
			v = t.v;
			halfTh = v(3) / 2;
			v(3) = halfTh;
			t1 = transition(t.id_from, newId, v, t.precision);

			% Create the second half
			t2 = transition(newId, t.id_to, [0;0;halfTh], t.precision);

			% Replace the old
			trans = [trans(1:i-1), t1, t2, trans(i+1:end)];

			% Adjust observations
			newO = observation(newId, 1, 0, 0); % NOTE: landmark 1 fake observation
			obs = [obs(1:i-1), newO, obs(i:end)];

			++i;
			++transLength;
		end

		++i;
	end

end
