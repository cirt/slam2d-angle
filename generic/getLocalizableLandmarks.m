%{
From a series of observations, it returns the list of all landmarks id that
are localizable. This happens when a landmark is observed 2 or three times.

Params:
	observations: array of struct of an observations (see loadG2o)

Returns:
	vector of landmarks IDs
%}

function landIDs = getLocalizableLandmarks(observations)

% Counting
global i2l;
global l2i;
counts = zeros(size(i2l));
for observation = observations
	for o = observation.observation
		counts(l2i(o.id)) += 1;
	end
end

% Test and return IDs
landIDs = zeros(0,0);
l = 1;
for i = 1:length(counts)
	if counts(i) >= 4
		landIDs(l) = i2l(i);
		l += 1;
	end
end

end
