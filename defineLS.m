%{
This function defines the least squares problem.
The problem is defined by the second order approximation of the cost function:
this method returns the gradient and hessian around point x. This implicitly
defines: state, measures and known quantities. The length of the observations
parameter determine the number of poses to be estimated (the same number as
posesN). Each transitions(i) must lead to the pose that generated
observations(i).

States are variables with 'x' prefix: see model/stateIdx.m for an explanation
of its content.
Observations are variables with 'z' prefix: They are annotated, relative
bearing of landmarks w.r.t the robot, and odometry measures (relative poses).
NOTE: landmarks not observed have diagonal blocks associated, to keep them
still. Even though the solver should exploit the sparse matrix H, one could
have removed these components entirely.

Params:
	x: the state at which the approximation is computed.
	observations: array of struct of an observations (see loadG2o)
	transitions: array of struct of odometry measures (see loadG20)

Returns:
	H: Hessian matrix at x of the cost function
	b: gradient at x of the cost function
	chi2: constant term at x
%}

function [H, b, chi2] = defineLS(x, observations, transitions)
	
	% Init
	N = length(x);
	H = sparse(N,N);
	b = sparse(N,1);
	chi2 = 0;

	% >> Cost: static landmarks

	% Select the landmarks to be localized
	landIDs = getLocalizableLandmarks(observations);
	global l2i;

	% Assign a precision to each landmark position
	landmRange = (stateIdx(-1)(1)):N;
	H(landmRange,landmRange) = speye(length(landmRange)) * 10;


	% For each transition/observation
	for i = 1:length(observations)

		% >> Cost: Bearing

		for o = observations(i).observation

			% Skip if this landmark is non localizable/reliable
			if !any(landIDs == o.id)
				continue;
			end

			% Extract the relevant variables from the state.
			%	  NOTE: observations(i).pose_id is in position i in the state vector
			range = stateIdx([i, -l2i(o.id)]);

			% Compute the bearing error
			[Hb, bb, chi2b] = approximateBearingError(x(range), o,
					observations(i).precision);

			% Add this term
			H(range,range) += Hb;
			b(range) += bb;
			chi2 += chi2b;
		end


		% >> Cost: Odometry

		% Skip the first (no previous state)
		if i == 1
			continue
		end

		% Extract the correct variables from the state.
		%	  NOTE: transitions(i).id_to is in position i in the state vector
		xTo = x(stateIdx(i));
		xFrom = x(stateIdx(i-1));
		t = transitions(i);

		% Compute the odometry error
		[Ho, bo, chi2o] = approximateOdometryError([xFrom; xTo], t);

		% Add this term
		H(stateIdx([i-1,i]), stateIdx([i-1,i])) += Ho;
		b(stateIdx([i-1,i])) += bo;
		chi2 += chi2o;

	end

end


%{
Computes the quadratic approximation of the cost function relative to a
single bearing measurement. Computes H,b,chi2 for the error function of the
bearing computed at x. The state x must be the portion of the state vector that
contains just the variables which influence this measure.  If landmark j id
observed from pose i, x should be
	[x_i, y_i, th_i, lx_j, ly_j]'

Params:
	x: the pose and landmark position at which the approximation is computed.
	observation: struct of a single observation (see loadG2o)
	precision: precision of this observation (scalar)

Returns:
	H: Hessian matrix at x of the cost function
	b: gradient at x of the cost function
	chi2: constant term at x
%}
function [H, b, chi2] = approximateBearingError(x, observation, precision)

	% Error function
	trueZ = observation.bearing;
	[z, J] = landmarkMeasureAndJacobian(x);
	e = z - trueZ;

	% Return
	H = J' * precision * J;
	b = J' * precision * e;
	chi2 = e * precision * e;

end


%{
Computes the quadratic approximation of the cost function relative to the
odometry measure. Computes H,b,chi2 for the error function of the odometry
computed at x. The state x must contain two consecutive robot poses from which
the odometry measure will be predicted:
	[x_i, y_i, th_i, x_{i+1}, y_{i+1}, th_{i+1}]'

Params:
	x: 6x1 vector of two poses.
	transition: struct of a single odometry measure (see loadG20). This is
			the actual value of the measure.

Returns:
	H: Hessian matrix at x of the cost function
	b: gradient at x of the cost function
	chi2: constant term at x
%}
function [H, b, chi2] = approximateOdometryError(x, transition)

	% Error function
	trueZ = transition.v;
	[z, J] = odometryMeasureAndJacobian(x);
	e = z - trueZ;
	
	% Return
	H = J' * transition.precision * J;
	b = J' * transition.precision * e;
	chi2 = e' * transition.precision * e;

end
