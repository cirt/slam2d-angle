# Probabilistic Robotics course: final project.
Student: **Roberto Cipollone**.
Title: **#4 Least Squares based 2D Bearing Only SLAM**.

Input: Dataset of labelled bearing readings, and odometry measurements
Output: Trajectory of the robot and map of landmarks
Method: Least squares estimation.

A demo is available at: [video](https://youtu.be/Xzw_qwXwdew "Youtube video")

### How to run
This software has been written in **Octave**. It can be executed by simply running the main script. For example with `octave main.m`. The input data is included in this repository for simplicitly. It consists of a g2o file with a list of odometry measurements in SE(2) and a list of bearing measurements. Bearing measures also contain the unique ID of the landmkark that generated it. This file is under `dataset/`.

During the execution, a **plot** shows the last estimate of the state vector. The black line (with dots) is a portion of the history of positions visited by the robot. The current robot position is the green square, with a blue arrow that indicates its orientation. Red dots are the true landmarks positions; while blue dots are the estimated landmarks positions.

Of course the robot doesn't know anything aobout the map, it can only perceive the noisy bearing angles of some visible landmarks. The goal of the robot is to localize itself and all  landmarks.

### The idea

The algorithm uses the **Gauss-Newton** algorithm to minimize a cost function defined on the state variables. For local algorithms, a good initialization is fundamental. It would be very hard to converge to the global optimum, given the whole problem. Instead, the program **iterates for each time instant** (as an online algorithm). This allows to use previous solutions to create good guesses for the initial state in the next timestep. At each step:
* Odometry is used to initialize the estimate of next pose of the robot, given the previous (good) solution.
* Bearings read in last N timesteps are used to initialize the landmarks positions through triangulation.

This allows the algorithm to converge even without any initial knowledge (landmarks are initially placed at random in a large circle).

The **state** vector is structured as:
```
x = [x_1, y_1, th_1, ..., x_N, y_N, th_N, lx1, ly2, ..., lxL, lyL]'
```
It contains N groups of parameters that store the last N robot configurations (actually, they are inverse transformations, and N is the most recent). At each instant, these are shifted a new prediction is added. The last L blocks are the landmark coordinates.

Given an initial guess, this vector is estimated with Gauss-Newton. The N poses are connected, in the cost function, by the odometry readings; while landmarks are connected to poses through the bearing measurements that are availabe at each of them.

### Structure of the software

All source files have comments. The purpose and usage of each of them is written in the initial comments. The `main` script initializes the program and iterates on the timesteps. `minimizeLS` implements the iterative minimization algorithm. `defineLS` computes the second-order approximation of the cost function. The `basics/` directory contains simple utility functions, such as rotations and transformation matrices. `model/` is the most important part: each function in it defines (and depends on) the shape of the state vector, the observation and transition models.

The convergence of the algorithm can be modified by changing some constants in `minimizeLS`.
In `main`, `maxPosesN` determines the maximum number of poses N that should be estimated.

### Efficiency

In my computer, each solution of the sparse linear sistem takes about 2ms with 374 variables (30 poses + 142 landmarks) (this also depends on the frequence of the observations). Each approximation of the cost function takes less than 0.3s. Plotting the solution requires more than 4s. However, since this is a simulation, I think that an expressive plot is important.
