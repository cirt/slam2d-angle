function out = drawParticles(samples, weights, best_particle, gt_pose)

	dim_particles = size(samples,2);

	holding(true);
	plot(samples(1,:), samples(2,:), 'b.');

	plot(gt_pose.x, gt_pose.y, 'c.');
	drawCircle(gt_pose.x, gt_pose.y, 0.5);

	if best_particle > 0
		drawCircle(samples(1,best_particle), samples(2,best_particle), 0.5);
		drawRobot(samples(:,best_particle));
	endif

	holding(false);

endfunction
