function done = drawLandmarks(land,color, mode)
	
	if(nargin == 1)
		color = 'r';
		mode = 'fill';
	end

	N = length(land);
	radius = 0.1;
	holding(true);
	for i=1:N
		drawShape('circle', [land(i).x_pose, land(i).y_pose, radius], mode, color);
		drawLabels(land(i).x_pose, land(i).y_pose, land(i).id, '%d');
	end
	holding(false);
end
