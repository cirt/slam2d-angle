function out = drawRobot(pose, covariance)

	holding(true);
	dim = 0.25;
	arr_len = 1;

	% Body
	drawShape('rect', [pose(1), pose(2), dim, dim, pose(3)], 'fill','g');

	% Direction
	dirX = pose(1) + arr_len*cos(pose(3));
	dirY = pose(2) + arr_len*sin(pose(3));
	drawArrow([pose(1),pose(2), dirX,dirY],
			1, dim, arr_len/2, 0.5); % NOTE: small line width

	holding(false);

end
