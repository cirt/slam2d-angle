%{
Utility function: sets 'hold on' or 'hold off' and unsets.
When set, the previous value is saved so that it can be restored.
Example:
	holding(true);
	..do stuff..
	holding(false);
Params:
	set: if true, the option is set, otherwise it is restored
	on: if true, hold is set to on, off otherwise (defaults to true)
%}

function holding(set, on)

	return
	% do nothing: removing all 'hold' commands

	% In other to use this function, previous must be a stack
	%persistent previous

	%if set % set
	%	previous = ishold;
	%	if nargin < 2
	%		on = true;
	%	end
	%	if on
	%		hold on
	%	else
	%		hold off
	%	end
	%else % unset
	%	if previous
	%		hold on
	%	else
	%		hold off
	%	end
	%end
end
