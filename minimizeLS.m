%{
Minimization of the least squares problem. Iterates until convergece.
The first constants can be modified in control termination conditions and
regularization.
See defineLS for an interpretation of most paramters.

Params:
	xInit: initial estimate of the optimal state.
	observations: array of struct of observations (see loadG2o)
	transitions: array of struct of odometry measures (see loadG20)
Returns:
	x: final state
	chi: weighted error at x (Mahalanobis distance)
	P: approximated precision matrix at x (assuming x is optimum)
	converged: true if the error has converged, false if timeout has occurred
%}

function [x, chi, P, converged] = minimizeLS(xInit, observations, ...
		transitions)

	% Parameters
	damping = 10000;  % High because measurements have an high precision in H
	maxIterations = 100;
	dChiConverged = 0.2; % If dChi is reduced by this less than this, converged
	chiConverged = 50; % If chi is reduced by this less than this, converged

	% History. Unused for now, but I keep it anyway
	xHist = zeros(length(xInit), 0);
	chiHist = zeros(1, 0);

	% Optimization loop
	i = 0; chi = Inf; dChi = Inf; x = xInit;
	while 1

		% Termination: timeout
		i = i + 1;
		if i > maxIterations
			printf('Stop. Max number of %i iterations reached.\n', maxIterations);
			converged = false;
			break
		end
		% Termination: converged
		if length(chiHist) >= 2
			dChi = chiHist(end-1) - chiHist(end);
		end
		if dChi >= 0 && dChi < dChiConverged && chi < chiConverged
			converged = true;
			break
		end

		% Print
		printf('Iteration %i\n', i);

		% Define the approximation
		[H, b, chi2] = defineLS(x, observations, transitions);
		chi = sqrt(chi2);

		% Minimize
		%tic();
		H = H + speye(size(H)) .* damping;
		dx = -H \ b;
		x = x + dx;
		%toc();

		% Save
		xHist(:,end+1) = x;
		chiHist(end+1) = chi;

		% Print
		printf('chi %f\n\n', chi); % Actually it's associated to the previous x
		fflush(stdout);

	end

% Return
P = H;

end
